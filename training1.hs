module Training1 where
--Basic drills

--Exercise1.1
factorial :: Integer -> Integer
factorial x =
  case x of
    0 -> 1
    _ -> x * factorial (x - 1)

--Exercise1.2
doubleFactorial :: Integer -> Integer
doubleFactorial x =
  case x of
    0 -> 1
    1 -> 1
    _ -> x * doubleFactorial (x - 2)

