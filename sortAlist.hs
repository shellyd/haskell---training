module SortList where
--import Data.List

data Tree a
  = Empty 
  | Node (Tree a) a (Tree a)
  deriving(Show,Eq)

--inorder
flatten :: (Ord a) => Tree a -> [a]
flatten tree =
  case tree of
    Empty               -> []
    (Node left v right) -> flatten left ++ [v] ++ flatten right 
  
insertElem ::(Ord a) => a -> Tree a -> Tree a
insertElem val Empty = Node Empty val Empty
insertElem val (Node left v right)
  | val == v = Node left v right
  | val  > v = Node left v (insertElem val right)
  | val  < v = Node (insertElem val left) v right
  
listToTree :: (Ord a) => [a] -> Tree a
listToTree x =
  case x of
    []     -> Empty
    [a]    -> Node Empty a Empty
    (a:as) -> insertElem a (listToTree as)





