module Training5 where

--import Data.List
import Data.Char

quickSort' :: (Ord a) => (a -> a -> Ordering) -> [a] -> [a]
quickSort' _  [] = []
quickSort' insensitive (x:xs) = (quickSort' insensitive less) ++ (x : equal) ++ (quickSort' insensitive more)

  where
   less  = filter (\y -> y `insensitive` x == LT) xs
   equal = filter (\y -> y `insensitive` x == EQ) xs
   more  = filter (\y -> y `insensitive` x == GT) xs
                  
insensitive :: String -> String -> Ordering
insensitive x y = compare (map toUpper x) (map toUpper y)

--for loop implementation in haskell
for :: a -> (a -> Bool) -> (a -> a) -> (a -> IO()) -> IO()
for i p f job =
 if p i
 then do
  job i
  for (f i) p f job
 else return()

 --sequenceIO implementation
sequenceIO :: [IO a] -> IO [a]
sequenceIO (x:xs) =
  case (x,xs) of
  (_,[]) -> return ([])
  (x,xs) ->  do x
                sequenceIO xs
 {-
sequenceIO :: [IO a] -> IO [a]
sequenceIO []     =  return ([])  
sequenceIO (x:xs) = do
  c  <- x
  cs <- sequenceIO xs
  return (c:cs)
-}
--mapIO implementation
mapIO :: (a -> IO b) -> [a] -> IO [b]
mapIO f (a:as) = case (a,as) of
  (_,[]) -> return([])
  (_,_)  -> do
   c  <- f a
   cs <- mapIO f as
   return(c:cs)

const' :: a -> b -> a
const' a _ = a

curry' :: ((a,b) -> c) -> a -> b -> c
curry' f x y = f (x,y)

uncurry' :: (a -> b -> c) -> (a,b) -> c
uncurry' f (x,y) = f x y

foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' _ acc [] = acc
foldr' f acc (x:xs) = f x (foldr f acc xs)

--Implement foldl with foldr - need explanation to this one
foldl_ :: (b -> a -> b) -> b -> [a] -> b
foldl_ f acc = foldr' (flip f) acc . reverse   

  
  

  

