module Training where
--Basic drills

--Exercise2.2
power :: Integer -> Integer -> Integer
power x y =
  case (x,y) of
  ( _, 0) -> 1
  ( _, 1) -> x
  ( _, _) -> x * (power x (y - 1))

--Exercise2.3
plusOne :: Integer -> Integer
plusOne x = x + 1

addition :: Integer -> Integer -> Integer
addition x y =
  case (x, y) of
    (_, 0) ->  x
    (_, _) -> plusOne (addition x (y - 1))

--Exercise2.4
log2 :: Integer -> Integer
log2 x =
  case x of
    1   -> 0
    _   -> 1 + log2 (div x 2)
   
