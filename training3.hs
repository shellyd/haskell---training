module Training3 where
--Basic drills

--Exercise1.1
replicate' :: Int -> a -> [a]
replicate' x a =
  case x of
   0 -> []
   _ -> a : replicate' (x - 1) a

--Exercise1.2
(!!!) :: [a] -> Int -> a
[]     !!! _ = error "empty list"
(s:_)  !!! 0 = s
(s:sx) !!! y = sx !!! (y -1)

--Exercise1.3
zip' :: [a] -> [b] -> [(a,b)]
zip' [x] [y]       = [(x,y)]
zip' [x] (y:ys)    = [(x,y)]
zip' (x:xs) (y:ys) = (x,y) : zip' xs ys

--Exercise1.4
len' :: [a] -> Int -> Int
len' [] acc     = acc
len' (a:ax) acc = len' ax (1 + acc)
