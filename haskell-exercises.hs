module Haskell_exercises where
import Data.List
--Lists
--1.Problem 1
--function that finds the last element of a list.
findLst :: [a] -> a
findLst lst =
  case lst of
    []     -> error "Empty list!!!"
    [x]    -> x
    (_:xs) -> findLst xs 

--2.Problem 2
--function that finds th elast but one element of a list
findLst1 :: [a] -> a
findLst1 lst  =
  case lst of
    []       -> error "Empty list"
    (x:xs)   -> if length xs == 1 then x 
                else findLst1 xs 
--3.Problem 3
--function that finds the k element of a list.
findElement :: [a] -> Int -> a
findElement a i =
  case (a,i) of
    ([], _)  -> error "Empty list"
    (_ , 0)  -> error "No such index"
    (x, 1)   -> head x   
    (_:ax,_) -> findElement ax (i-1) 

--4.Problem 4
--Find the number of elements of a list.
myLen :: [a] -> Int 
myLen = sum . map (\_ -> 1)
{-
myLen a acc =
  case (a, acc) of
    ([], acc) -> acc
    (_:ax,_)  -> myLen ax (acc + 1) 
-}
--5.Problem 5
--Reverse a list.
myRev :: [a] -> [a]
myRev a =
  case a of
    []     -> []
    (x:xs) -> myRev xs ++ [x]
    
--6.Problem 6    
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome a =
  case a of
    []     -> True
    [_]    -> True
    (x:xs) -> if x == last xs
              then isPalindrome (drop 1 (reverse xs))
              else False
--8.Problem 8
--Eliminate consecutive duplicates of list elements
compress ::(Eq a) => [a] -> [a]
compress a =
  case a of
    []     -> []
    [x]    -> [x]
    (x:xs) -> if x == head xs then compress xs
              else x : compress xs
--7.Problem 7
data NesList a
  = Elem a
  | List [NesList a]

flatten :: NesList a -> [a]
flatten (Elem x) = [x]
flatten (List xs) = concatMap flatten xs   
    
--9.Problem 9
--Pack consecutive duplicates of list elements into sublists.
pack ::(Eq a) => [a] -> [[a]]
pack a =
  case a of
    []     -> [] 
    (x:xs) -> (x : takeWhile (==x) xs) : pack (dropWhile (==x) xs) 
--  (x:xs) -> break (/=x) xs 

--10.Problem 10
--Run-length encoding of a list.
encode :: (Eq a) => [a] -> [(Int,a)]
encode = map (\x -> (length x, head x)) . group
