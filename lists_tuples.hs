module Training4 where

--Exercise2.1-lists
head' :: [a] -> a
head' (x:xs) = x
head' [] =  error "empty list"
  
tail' :: [a] -> [a]
tail' (x:xs) = xs
tail' [] = error "empty list"
 
--Exercise2.2-list_head
fifthElement :: [a] -> a
fifthElement a = head' ( tail' (tail' (tail' (tail' a))))
