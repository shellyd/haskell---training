module Haskell_problems where
import Data.List

encode :: (Eq a) => [a] -> [(Int,a)]
encode = map (\x -> (length x, head x)) . group

--Problem 11
data ListItem a
  = Single a
  | Multiple Int a
  deriving (Show)

encodeMod :: Eq a => [a] -> [ListItem a]
encodeMod = map encodeHelper . encode
  where
    encodeHelper (1,x) = Single x
    encodeHelper (n,x) = Multiple n x

--Problem 12
decodeMod :: [ListItem a] -> [a]
decodeMod = concatMap decodeHelper
  where
    decodeHelper (Single x) = [x]
    decodeHelper (Multiple n x) = replicate n x

--Problem 13
encodeD :: (Eq a) => [a] -> [ListItem a]
encodeD [] = []
encodeD (x:xs)
  | count == 1 = Single x : encodeD xs
  | otherwise  = Multiple count x : encodeD rest
  where
    (matched, rest) = span (==x) xs
    count = 1 + length matched
    
--Problem 14
dupli :: [a] -> [a]
dupli a =
  case a of
    []     -> []
    (x:xs) -> x : x : dupli xs

--Problem 15
repli :: [a] -> Int -> [a]
repli a x =
  case (a,x) of
    (_,0) -> []
    (y,n) -> concatMap (replicate n) y

--Problem 16
dropEvery :: [a] -> Int -> [a]
dropEvery a x =
  case (a,x) of
    ([],_)   -> []
    (list,n) -> take (n-1) list ++ dropEvery (drop n list) n

      --Problem 17
split :: [a] -> Int -> ([a],[a])
split a i =
  case (a,i) of
    ([],_)   -> error "can't split a empty list"
    (list,0) -> ([],list)
    (list,n) -> (take n list, drop n list) 

--Problem 18
slice :: [a] -> Int -> Int -> [a]
slice a x y =
  case (a,x,y) of
    ([],_,_)  -> []
    (lst,i,j) -> drop (i - 1) (take j lst) 

--Problem 19
rotate :: [a] -> Int -> [a]
rotate list x =
  case (list,x) of
    ([],_)  -> []
    (lst,i) -> if i > 0 then slice lst (i + 1) (length lst) ++ take i lst
               else drop (length lst - abs') lst  ++ take (length lst - abs') lst   
      where
        abs' = i * (-1)

--Problem 20
removeAt :: Int -> [a] -> (a,[a])
removeAt a x =
  case (a,x) of
    (_,[])   -> error "empty list"
    (0,_)    -> error "no such index"
    (i,list) -> (list !! (i-1), take (i-1) list ++ drop  i list) 
