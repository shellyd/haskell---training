module Game where

import System.IO

data Move
  = Rock
  | Paper
  | Scissors
  deriving (Eq, Read, Show, Enum, Bounded)

data Outcome
  = Lose
  | Tie
  | Win
  deriving (Show, Eq, Ord)

outcome :: Move -> Move -> Outcome
outcome Rock Paper     = Lose
outcome Paper Scissors = Lose
outcome Scissors Rock  = Lose
outcome x y
  | x==y      = Tie
  | otherwise = Win
  
parseMove :: String -> Maybe Move
parseMove "Rock"      = Just Rock
parseMove "Paper"     = Just Paper
parseMove "Scissors"  = Just Scissors
parseMove _           = Nothing

parseMove' :: String -> Maybe Move
parseMove' str | [(m, "")] <- reads str = Just m
               | otherwise              = Nothing

getMove :: Handle -> IO Move
getMove h = do
  hPutStrLn h $ "Please enter one of " ++ show (([minBound..]) :: [Move])
  input <- hGetLine h
  case parseMove input of
    Just move -> return move
    Nothing   -> getMove h

computerVsUser :: Move -> Handle -> IO ()
computerVsUser computerMove h = do
  userMove <- getMove h
  let o = outcome userMove computerMove
  hPutStrLn h $ "You " ++ show o

